## 工程用来存放通用的图片资源

### 1.markdown引用该工程中的图片的方式

在码云上点开图片的查看页，点击左侧的**原始数据**在弹出的窗口中复制浏览器的url,该url即为在markdown中引用图片时使用的链接，如下图

![](https://gitee.com/zhituaishangc/image-sources/raw/master/common/%E5%A6%82%E4%BD%95%E5%9C%A8markdown%E4%B8%AD%E5%BC%95%E7%94%A8%E5%9B%BE%E7%89%87.png)

